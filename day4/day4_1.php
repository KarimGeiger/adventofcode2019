<?php

$begin = 138307;
$end = 654504;

$possibilities = 0;

for ($i = $begin; $i < $end; $i++) {
    $split = str_split($i);
    $last = null;
    $valid = false;

    foreach ($split as $pos) {
        if ($last === null) {
            $last = $pos;
            continue;
        }
        if ($pos < $last) {
            continue 2;
        }
        if ($pos === $last) {
            $valid = true;
        }
        $last = $pos;
    }

    if ($valid) {

        $possibilities++;
    }
}

echo $possibilities . "\n";