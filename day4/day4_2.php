<?php

$begin = 138307;
$end = 654504;

$possibilities = 0;

for ($i = $begin; $i < $end; $i++) {
    $split = str_split($i);
    $last = null;
    $valid = false;
    $matchGroups = [];

    foreach ($split as $pos) {
        if ($last === null) {
            $last = $pos;
            continue;
        }
        if ($pos < $last) {
            continue 2;
        }
        if ($pos === $last) {
            $valid = true;
            $matchGroups[] = $pos;
        }
        $last = $pos;
    }

    if ($valid) {
        $doubleValid = false;
        foreach (array_unique($matchGroups) as $group) {
            if (strpos($i, $group . $group . $group) === false) {
                $doubleValid = true;
            }
        }
        if ($doubleValid) {
            $possibilities++;
        }
    }
}

echo $possibilities . "\n";