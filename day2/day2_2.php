<?php

include_once __DIR__ . '/src/OpAdd.php';
include_once __DIR__ . '/src/OpMultiply.php';
include_once __DIR__ . '/src/OpHalt.php';

$code = array_map('intval', array_filter(explode(',', file_get_contents(__DIR__ . '/input.txt')), function ($val) {
    return trim($val) !== '';
}));


// brute-force!
for ($i = 0; $i <= 99; $i++) {
    for ($j = 0; $j <= 99; $j++) {
        $newCode = $code;
        $newCode[1] = $i;
        $newCode[2] = $j;
        $newResponse = execute($newCode);
        if ($newResponse[0] === 19690720) {
            die((100 * $i + $j) . "\n");
        }
    }
}


function execute($code)
{
    for ($i = 0; $i < count($code); $i++) {
        $opcode = getOpCode($code, $i);

        $opcode->execute();
        $code = $opcode->getCode();

        if ($opcode->shouldHalt()) {
            return $code;
        }

        // Advance counter by parameter count to get to next opcode
        $i += $opcode->getParameterCount() + 1;
    }

    return $code;
}

function getOpCode(array $code, int $i): OpCode
{
    switch ($code[$i]) {
        case 1:
            return new OpAdd($code, $i);
        case 2:
            return new OpMultiply($code, $i);
        case 99:
            return new OpHalt($code, $i);
        default:
            throw new Exception('Invalid opcode.');
    }
}