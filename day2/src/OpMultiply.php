<?php

include_once __DIR__ . '/OpCode.php';

class OpMultiply extends OpCode
{
    public function getParameterCount(): int
    {
        return 2;
    }

    public function execute()
    {
        $params = $this->getParameters();

        $this->code[$this->code[$this->position + 3]] = $params[0] * $params[1];
    }

}