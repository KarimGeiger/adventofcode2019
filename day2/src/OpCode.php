<?php


abstract class OpCode
{
    /** @var array */
    protected $code;
    /** @var int */
    protected $position;

    public function __construct(array $code, int $position)
    {
        $this->code = $code;
        $this->position = $position;
    }

    public abstract function execute();

    public abstract function getParameterCount(): int;

    public function shouldHalt(): bool
    {
        return false;
    }

    public function getCode(): array
    {
        return $this->code;
    }

    protected function getParameters(): array
    {
        $params = [];

        for ($i = 0; $i < $this->getParameterCount(); $i++) {
            $params[] = $this->code[$this->code[$this->position + $i + 1] ?? 1000000] ?? null;
        }

        return $params;
    }
}