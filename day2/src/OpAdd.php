<?php

include_once __DIR__ . '/OpCode.php';

class OpAdd extends OpCode
{
    public function getParameterCount(): int
    {
        return 2;
    }

    public function execute()
    {
        $this->code[$this->code[$this->position + 3]] = array_sum($this->getParameters());
    }
}