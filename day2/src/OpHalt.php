<?php

include_once __DIR__ . '/OpCode.php';

class OpHalt extends OpCode
{
    public function getParameterCount(): int
    {
        return 0;
    }

    public function execute()
    {
        // noop
    }

    public function shouldHalt(): bool
    {
        return true;
    }

}