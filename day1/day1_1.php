<?php

use Tightenco\Collect\Support\Collection;

include_once __DIR__ . '/vendor/autoload.php';

echo (new Collection(explode("\n", file_get_contents(__DIR__ . '/input.txt'))))
    ->filter()
    ->map(function ($val) {
        return floor(trim($val) / 3) - 2;
    })
    ->sum();

echo "\n";