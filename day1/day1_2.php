<?php

use Tightenco\Collect\Support\Collection;

include_once __DIR__ . '/vendor/autoload.php';

function calc_fuel($in)
{
    return floor($in / 3) - 2;
}

echo (new Collection(explode("\n", file_get_contents(__DIR__ . '/input.txt'))))
    ->filter()
    ->map(function ($val) {
        $fuel = $newFuel = calc_fuel(trim($val));
        do {
            $newFuel = calc_fuel($newFuel);
            $fuel += max(0, $newFuel);
        } while ($newFuel > 0);

        return $fuel;
    })
    ->sum();

echo "\n";